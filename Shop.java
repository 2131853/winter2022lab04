import java.util.Scanner;
public class Shop {

	//MAIN METHOD
	public static void main(String[] args) {
		//INITIALIZING THE SCANNER AND SONG OBJECTS
		Scanner reader = new Scanner(System.in);
		Song[] songs = new Song[4];
		
		//FOR LOOP FOR FILLING ARRAY FIELDS
		for( int i = 0; i <= songs.length - 1; i++) {
			
			//ASKING FOR FIELDS AND FILLING THEM OUT
			System.out.println("Insert song title:");
			String newTitle = reader.nextLine();
			
			System.out.println("Insert the artist of the song:");
			String newArtist = reader.nextLine();
			
			System.out.println("Insert the number of plays here");
			int newPlays = Integer.parseInt(reader.nextLine());
			
			songs[i] = new Song(newTitle, newArtist, newPlays);
		}
		
		//PRINTING LAST SONG FIELDS BEFORE UPDATE
		System.out.println("Title of last song is: " + songs[songs.length - 1].getTitle());
		System.out.println("Artist of last song is: " + songs[songs.length - 1].getArtist());
		System.out.println("The number of plays of the last song is: " + songs[songs.length - 1].getPlays());
		
		
		//UPDATING FIELDS WITH REMAINING SETTER METHODS
		System.out.println("Insert song title for last song");
		songs[songs.length - 1].setTitle(reader.nextLine());
		
		System.out.println("Insert number of plays of last song");
		songs[songs.length - 1].setPlays(Integer.parseInt(reader.nextLine()));
		
		//PRINTING LAST SONGS FIELDS AFTER UPDATES
		System.out.println("Title of last song is: " + songs[songs.length - 1].getTitle());
		System.out.println("Artist of last song is: " + songs[songs.length - 1].getArtist());
		System.out.println("The number of plays of the last song is: " + songs[songs.length - 1].getPlays());
	
		//CALLING THE INSTANCE METHOD ON THE LAST SONG
		songs[songs.length-1].reaction();
	}
}