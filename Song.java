public class Song {
	
	//INITIALIZING MY FIELDS
	private String artist;
	private String title;
	private int plays;
	
	//CONSTRUCTOR
	public Song(String newTitle, String newArtist, int newPlays){
		this.artist = newArtist;
		this.title = newTitle;
		this.plays = newPlays;
	}
	
	
	//GETTER METHODS
	public String getArtist(){
		return this.artist;
	}
	
	public String getTitle(){
		return this.title;
	}
	
	public int getPlays(){
		return this.plays;
	}
	
	//SETTER METHODS
	public void setTitle(String newTitle){
		this.title = newTitle;
	}
	
	public void setPlays(int newPlays){
		this.plays = newPlays;
	}
	
	//CREATING MY INSTANCE METHOD 
	public void reaction() {
		//IF THE ARTIST IS BTS IT PRINTS "Wow!"
		//OTHERWISE IT PRINTS "Cool!"
		if (this.artist.equals("bts")) {
			System.out.println("Wow!");
		} else {
			System.out.println("Cool!");
		}
	}
}